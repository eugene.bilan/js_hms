
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */


 const buttonContainer = document.getElementById('buttonContainer');
 const tabContainer = document.getElementById('tabContainer');
 const contentTabs = tabContainer.querySelector('.tab');
 const arrayTabs = Array.from(tabContainer.children);
 buttonContainer.insertAdjacentHTML('beforeend', `<button type="text" class="hideAllTabs" onClick="hideAllTabs()">hide All Tabs</button>`);
 buttonContainer.onclick = function(ev) {
   if('button' === ev.target.type) {
     arrayTabs.forEach(elem => {
       elem.classList.remove('active');
       if(ev.target.dataset.tab === elem.dataset.tab) {
         elem.classList.add('active');
       }
     });
   }
 };
 
 function hideAllTabs() {
   arrayTabs.forEach(elem => {
     elem.classList.remove('active');
   });
 }